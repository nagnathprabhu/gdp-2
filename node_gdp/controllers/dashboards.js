var express = require('express');
var api = express.Router();
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";

//GET Index for dashboard
api.get("/", function (request, response) {
  var data = request.app.locals.issueLogs.query;
  console.log(data)
  var high = 0;
  var low = 0;
  var medium = 0 ;
  var hightime = 0.0;
  var lowtime = 0.0;
  var mediumtime = 0.0;
  var rtim = 0.0;
  var dstim = 0.0;
  var dvtim = 0.0;
  var tstim = 0.0;
  
        for (var i = 0; i < data.length; i++){
          switch(data[i].priority) {  //switch case for different times
   case "Low": low = low + 1;
   lowtime = lowtime+data[i].totalTime;
   break;
   case "Medium": medium = medium + 1;
   mediumtime = mediumtime+data[i].totalTime;
   break;
   case "High": high = high + 1;
   hightime = hightime+data[i].totalTime;
   break;
   default : console.log("No match found");
   
   }
   //declarations of different types of phases
   rtim = rtim+data[i].reqTime;
   dstim = dstim+data[i].designTime;
   dvtim = dvtim+data[i].developmentTime;
   tstim = tstim+data[i].testingTime;

        }
        console.log("requirement time" + rtim);
  response.render("dashboard/index.ejs", {high : high , medium : medium , low : low, hightime : hightime , mediumtime : mediumtime , lowtime : lowtime , rtim : rtim , dstim : dstim , dvtim : dvtim , tstim : tstim});
});
module.exports = api;