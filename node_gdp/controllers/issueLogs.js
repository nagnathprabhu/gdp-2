var express = require('express');
var api = express.Router();
var find = require('lodash.find');
var remove = require('lodash.remove');
var findIndex = require('lodash.findindex');
var Model = require('../models/issueLog.js');
const notfoundstring = 'No such issues';
const fs = require('fs');

//const file = "/data/issueLogs.json";

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/issuelogs";
// HANDLE JSON REQUESTS --------------------------------------------


//GET create 
api.get('/create', function (req, res) {
    console.log('Handling GET /create' + req);
    res.render("issue_logs/create.ejs",
        { title: "Issues", layout: "layout.ejs" });
});
//GET Index 
api.get("/", function (request, response) {
    response.render("issue_logs/index.ejs", { title: 'Issues' });
});
//GET findall 
api.get('/findall', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    var data = req.app.locals.issueLogs.query;
    res.send(JSON.stringify(data));
});
//GET findone 
api.get('/findone/:id', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    var id = parseInt(req.params.id);
    var data = req.app.locals.issueLogs.query; // query the model
    var item = find(data, { '_id': id });
    if (!item) { return res.end(notfoundstring); }
    res.send(JSON.stringify(item));
});

//Delete an issue with a particular id
api.get('/delete/:id', function (req, res) {
    console.log("Handling GET /delete/:id " + req);
    var id = parseInt(req.params.id);
    var data = req.app.locals.issueLogs.query;
    var item = find(data, { '_id': id });
    if (!item) { return res.end(notfoundstring); }
    console.log("RETURNING VIEW FOR" + JSON.stringify(item));
    return res.render('issue_logs/delete.ejs',
        {
            title: "RB",
            layout: "layout.ejs",
            issueLog: item
        });
});
// GET the details pertaining to a particular issue
api.get('/details/:id', function (req, res) {
    console.log("Handling GET /details/:id " + req);
    var id = parseInt(req.params.id);
    var data = req.app.locals.issueLogs.query;
    var item = find(data, { '_id': id });
    if (!item) { return res.end(notfoundstring); }
    console.log("RETURNING VIEW FOR" + JSON.stringify(item));
    return res.render('issue_logs/details.ejs',
        {
            title: "Issues",
            layout: "layout.ejs",
            issueLog: item
        });
});

// GET /edit:/id
api.get('/edit/:id', function (req, res) {
    console.log("Handling GET /edit/:id " + req);
    var id = parseInt(req.params.id);
    var data = req.app.locals.issueLogs.query;
    var item = find(data, { '_id': id });
    if (!item) { return res.end(notfoundstring); }
    console.log("RETURNING VIEW FOR" + JSON.stringify(item));
    return res.render('issue_logs/edit.ejs',
        {
            title: "RB",
            layout: "layout.ejs",
            issueLog: item
        });
});

//for acctivate


api.get('/active/:id/:ison', function (req, res) {
    console.log("Handling POST /active/:id/:ison " + req);
    var id = parseInt(req.params.id);
    var ison = req.params.ison == "true" ? true : false;
    var data = req.app.locals.issueLogs.query;
    var item = find(data, { '_id': id });
    if (!item) { return res.end(notfoundstring); }
    console.log("RETURNING VIEW FOR" + JSON.stringify(item));
    item.isactive = ison;
    res.redirect("/issueLog");
});



// SAVE for creating new issues
api.post('/save', function (req, res) {

    console.log("Handling POST " + req);
    var data = req.app.locals.issueLogs.query;
    var item = new Model;
    console.log("NEW ID " + req.body._id);
    item._id = parseInt(req.body._id);
    item.name = req.body.name;
    item.description = req.body.description;
    item.priority = req.body.priority;
    item.status = req.body.status;
    item.reqTime = req.body.reqTime;
    item.designTime = req.body.designTime;
    item.developmentTime = req.body.developmentTime;
    item.testingTime = req.body.testingTime;
    item.totalTime = req.body.totalTime;
    data.push(item);




   /*  var obj = {
        "_id": parseInt(req.body._id),
        "name": req.body.name,
        "description": req.body.description,
        "priority": req.body.priority,
        "status": req.body.status,
        "isactive": false,
        "reqTime": req.body.reqTime,
        "designTime": req.body.designTime,
        "developmentTime": req.body.developmentTime,
        "testingTime": req.body.testingTime,
        "totalTime": req.body.totalTime
    }

    fs.readFile('./data/issueLogs.json', (err, data) => {
        if (err && err.code === "ENOENT") {
            // But the file might not yet exist.  If so, just write the object and bail
            return fs.writeFile('./data/issueLogs.json', JSON.stringify([data]), error => console.error);
        }
        else if (err) {
            // Some other error
            console.error(err);
        }    
        // 2. Otherwise, get its JSON content
        else {
            try {
                const fileData = JSON.parse(data);
    
                // 3. Append the object you want
                fileData.push(obj);
    
                //4. Write the file back out
                return fs.writeFile('./data/issueLogs.json', JSON.stringify(fileData), error => console.error)
            } catch(exception) {
                console.error(exception);
            }
        }
    }); */


    console.log("SAVING NEW ITEM " + JSON.stringify(item));
    return res.redirect('/issueLog');


});

// save for modifying the issues
api.post('/save/:id', function (req, res) {
    console.log("Handling SAVE request" + req);
    var id = parseInt(req.params.id);
    console.log("Handling SAVING ID=" + id);
    var data = req.app.locals.issueLogs.query;
    var item = find(data, { '_id': id });
    if (!item) { return res.end(notfoundstring); }
    console.log("ORIGINAL VALUES " + JSON.stringify(item));
    console.log("UPDATED VALUES: " + JSON.stringify(req.body));
    item.name = req.body.name;
    item.description = req.body.description;
    item.priority = req.body.priority;
    item.status = req.body.status;
    item.reqTime = req.body.reqTime;
    item.designTime = req.body.designTime;
    item.developmentTime = req.body.developmentTime;
    item.testingTime = req.body.testingTime;
    item.totalTime = req.body.totalTime;
    console.log("SAVING UPDATED ITEM " + JSON.stringify(item));
    return res.redirect('/issueLog');
    next();
});

//Delete an issue with a particular id
api.post('/delete/:id', function (req, res, next) {
    console.log("Handling DELETE request" + req);
    var id = parseInt(req.params.id);
    console.log("Handling REMOVING ID=" + id);
    var data = req.app.locals.issueLogs.query;
    var item = remove(data, { '_id': id });
    if (!item) { return res.end(notfoundstring); }
    console.log("Deleted item " + JSON.stringify(item));
    return res.redirect('/issueLog');
});

module.exports = api;