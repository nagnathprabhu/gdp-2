var express = require('express');
var api = express.Router();

/* GET default (home page) */
api.get('/', function (request, response, next) {
  response.render('issue_logs/index.ejs', { title: 'Bug Tracker' });
});



module.exports = api;