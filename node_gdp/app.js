//module dependencies
const http = require('http');
const express = require('express');
var loadash = require('lodash');
const path = require("path");
const logger = require("morgan");
const bodyParser = require("body-parser");
const engines = require('consolidate');
const compression = require('compression');
const port = 8080;
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const expressStatusMonitor = require('express-status-monitor');
const chalk = require('chalk');
const session = require('express-session');
const errorHandler = require('errorhandler');
const MongoStore = require('connect-mongo')(session);
global.app = express();
app.set('port', process.env.PORT || port);
app.use(express.static(__dirname + '/assets/'));

app.use(bodyParser.urlencoded({ extended: false }));

//Configuring the global config files
global.routes = require('./config/routes');
global.verify = require('./config/verify');
global.loadSeedData = require('./config/loadData');
loadSeedData.load();

verify.sampleDataImport();

// Connection to MongoDB
mongoose.connect('mongodb://localhost/issuelogs');

// set the root view folder & specify the view engine 
app.set("views", path.join(__dirname, "views"));
app.engine('ejs', engines.ejs);
app.set("view engine", "ejs");
app.use(expressStatusMonitor());
app.use(compression());

app.use(logger('combined'));
app.use(session({
  secret: 'cat',
}));

app.use(express.static(__dirname + '/assets/'));  // works for views in root view folder
app.use(expressLayouts);
routes.defineRoutes();
app.use(errorHandler());


app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});

module.exports = app;
