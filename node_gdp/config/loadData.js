const Datastore = require('nedb');

module.exports = {
    load: function () {

        var db = new Datastore();
        db.loadDatabase();
        global.issueLogs = require('../data/issueLogs.json');
        db.insert(issueLogs);
        // intialize app.locals (these objects will be available to our controllers)
        app.locals.issueLogs = db.find(issueLogs);


    }

}