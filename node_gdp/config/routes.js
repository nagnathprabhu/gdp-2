global.homeController = require('../controllers/home');
global.apiController = require('../controllers/api');
module.exports = {
    defineRoutes: function () {
        // Request to this URI will be handled by this CONTROLLER..........
        app.use('/', require('../controllers/index.js'));
        app.use('/issueLog', require('../controllers/issueLogs.js'));
        app.use('/issueLog/save', require('../controllers/issueLogs.js'));
        app.use('/issueLog/save/id', require('../controllers/issueLogs.js'));
        app.use('/dashboard', require('../controllers/dashboards.js'));
    }
}