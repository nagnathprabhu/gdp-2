var mongoose = require('mongoose')
  , Schema = mongoose.Schema

var IssueLogSchema = new Schema({
  _id: { type: Number, required: true },
  name: { type: String, required: true },
  description: { type: String, required: true },
  priority: { type: String, required: true },
  status: { type: String, required: true },
  isActive: { type: Boolean, required: true, default: true },
  reqTime: { type: Number, default: 0.0 },
  designTime: { type: Number, default: 0.0 },
  developmentTime: { type: Number, default: 0.0 },
  testingTime: { type: Number, default: 0.0 },
  totalTime: { type: Number, default: 0.0 }
})

var issueLog = mongoose.model('issueLog', IssueLogSchema)
module.exports = issueLog 
