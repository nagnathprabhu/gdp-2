# Bug Tracker

An issue tracking system built using: 

- Node.js platform
- Express web framework
- EJS templating engine
- MVC design pattern
- Mongoose mongodb object modeling for node.js
- BootStrap framework for responsive clients

# Prerequisites

Following must be installed to run this application

1. MongoDB
2. Node.js V6.0+



# Initialize (do these just once when setting up the project)

Clone a copy of repo on your machine

Open a command window in this project folder

Install nodemon globally to enable live updates.

```
> npm install -g nodemon
```

# Install dependencies as needed before running the app

Run npm install to install all the dependencies in the package.json file 
once before you begin and as new dependencies are added.

```
> npm install
```


# Start up Mongodb

Do create folder in C dirve as: C:\data\db

- Click the windows icon in the lower left of your screen
- Right-click "computer" and click properties
- In the nav on the left side of the page click on "Advanced system settings"
- Click Environment Variables
- Under the user section click PATH so that the section is highlighted and click edit
- In variable value go all the way to the end of the text and add ";C:\Program Files\MongoDB\Server\3.4\bin" to it

Right-click on the project folder and open Git Bash. Run mongod to start MongoDB.  (When done working the application, you can hit CTRL-C to stop.)

```
$ mongod 
```

# Run the app

In Windows, right-click on the project folder and "Open Command Window Here as Administrator". At the prompt, type nodemon app to start the server.  (CTRL-C to stop.)

```
> nodemon app
```

Point your browser to `http://localhost:8080` 
