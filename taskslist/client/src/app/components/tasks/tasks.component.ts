import { Component } from '@angular/core';
import {TaskService} from '../../services/task.service';
import {Task} from '../../../Task';

@Component({
  moduleId: module.id,  
  selector: 'tasks',
  templateUrl: 'tasks.component.html',
})
export class TasksComponent  {

    tasks:Task[];

    constructor(private taskSevice: TaskService){
        this.taskSevice.getTasks()
            .subscribe(tasks =>{
                console.log(tasks);
                this.tasks=tasks;
            });

    }
 }